<?php
namespace Elidev\Repository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class EventServiceProvider
 * @package Elidev\Repository\Providers
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Elidev\Repository\Events\RepositoryEntityCreated' => [
            'Elidev\Repository\Listeners\CleanCacheRepository'
        ],
        'Elidev\Repository\Events\RepositoryEntityUpdated' => [
            'Elidev\Repository\Listeners\CleanCacheRepository'
        ],
        'Elidev\Repository\Events\RepositoryEntityDeleted' => [
            'Elidev\Repository\Listeners\CleanCacheRepository'
        ],
        'Elidev\Repository\Events\RepositoryEntityRestored' => [
            'Elidev\Repository\Listeners\CleanCacheRepository'
        ]
    ];

    /**
     * Register the application's event listeners.
     *
     * @return void
     */
    public function boot()
    {
        $events = app('events');

        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        //
    }

    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return $this->listen;
    }
}
