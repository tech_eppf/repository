<?php

namespace Elidev\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

class TransferItems extends Model
{
    protected $fillable = ['original_id', 'target_id', 'type', 'target_client_id'];
}
