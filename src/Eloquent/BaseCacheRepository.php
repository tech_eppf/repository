<?php
namespace Elidev\Repository\Eloquent;

use Elidev\Repository\Traits\CacheableRepository;
use Illuminate\Container\Container as Application;

abstract class BaseCacheRepository extends BaseRepository {

    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheOnly = [];

    /**
     * @var string
     */
    protected $cachePrefix = '';

    public function __construct(Application $app)
    {
        parent::__construct($app);
    }


    /**
     * Get cacheOnly variable
     *
     * @return array
     */
    public function getCacheOnlyVariable()
    {
        return $this->cacheOnly;
    }
}
