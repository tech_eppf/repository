<?php

namespace Elidev\Repository\Eloquent;

use DB;
use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryCriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;
use Elidev\Repository\Criterias\FilterByIdOrSlug;
use Elidev\Repository\Events\RepositoryEntityCreated;
use Elidev\Repository\Events\RepositoryEntityDeleted;
use Elidev\Repository\Events\RepositoryEntityUpdated;
use Exception;
use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseRepository implements RepositoryInterface, RepositoryCriteriaInterface
{

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Collection of Criteria
     *
     * @var Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;

    /**
     * @var \Closure
     */
    protected $scopeQuery = null;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->criteria = new Collection();
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    abstract public function model();

    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        $this->applyCriteria();

        if ($this->model instanceof Builder) {
            $results = $this->model->get($columns);
        } else {
            $results = $this->model->all($columns);
        }
        $this->resetModel();

        return $results;
    }

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null $limit
     * @param array $columns
     * @param string $method
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        $this->applyCriteria();

        if ($limit == 'all') {
            $results = $this->model->get($columns);
        } else {
            $limit = is_null($limit) ? config('elidev-repository.pagination.limit', 15) : $limit;
            $results = $this->model->{$method}($limit, $columns);
            $results->appends(app('request')->query());
        }
        $this->resetModel();

        return $results;
    }

    /**
     * @Deprecated
     */
    public function getLimitOffset($limit = null, $offset, $columns = ['*'], $page = 1)
    {
        $this->applyCriteria();
        $limit = is_null($limit) ? config('elidev-repository.pagination.limit', 15) : $limit;
        return $this->model->offset($offset + ($page - 1) * $limit)->limit($limit)->get($columns);
    }

    /**
     * Get results in limitation with page and offset
     * @param array $columns
     * @param null $limit
     * @param int $offset
     * @param int $page
     * @return \Eloquent
     */
    public function getLimitWithPageOffset($columns = ['*'], $limit = null, $offset = 0, $page = 1)
    {
        $this->applyCriteria();
        $limit = is_null($limit) ? config('elidev-repository.pagination.limit', 15) : $limit;
        $offset = $offset + ($page - 1) * $limit;

        $eloquentCollection = $this->model->limit($limit);
        $total = $eloquentCollection->count();
        $eloquentCollection = $eloquentCollection->offset($offset);
        $output = $eloquentCollection->get($columns);
        $output->total = $total >= $offset ? $total - $offset : 0;
        $output->limit = $limit;
        $output->page = $page;
        $output->lastPage = ($output->total - $output->total % $limit) / $limit + 1;

        if ($page) {
            $output->hasMore = ($page * $limit) < $output->total;
        } else {
            $output->hasMore = $offset + $limit < $output->total;
        }


        return $output;
    }

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*'])
    {
        return $this->paginate($limit, $columns, "simplePaginate");
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     * @param bool $withoutCache
     *
     * @return mixed
     */
    public function find($id, $columns = ['*'], $withoutCache = false)
    {
        $this->resetCriteria();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by id including soft delete data
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findIncludeSoftDelete($id, $columns = ['*'])
    {
        return $this->model->select($columns)->withTrashed()->where('id', $id)->first();
    }

    /**
     * Find soft deleted data by id
     *
     * @param integer $id
     * @param array $columns
     *
     * @return Object Model
     */
    public function findSoftDelete($id, $columns = ['*'])
    {
        return $this->model->select($columns)->onlyTrashed()->where('id', $id)->first();
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findOrFail($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model->findOrFail($id, $columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value = null, $columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model->where($field, '=', $value)->get($columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyConditions($where);

        $model = $this->model->get($columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereWithoutCache(array $where, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyConditions($where);

        $model = $this->model->get($columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Count where
     *
     * @param array $where
     *
     * @return integer
     */
    public function countWhere(array $where)
    {
        $this->applyCriteria();
        $this->applyConditions($where);

        $count = $this->model->count();
        $this->resetModel();

        return $count;
    }

    /**
     * Find by ids
     *
     * @param array $ids
     *
     * @return mixed
     */
    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return new Collection([]);
        }

        $this->resetCriteria();
        $model = $this->model->whereIn('id', $ids)->get();
        $this->resetModel();

        return $model;
    }

    /**
     * Count
     *
     * @return integer
     */
    public function count()
    {
        $this->applyCriteria();

        $count = $this->model->count();
        $this->resetModel();

        return $count;
    }

    /**
     * Find wheres model
     *
     * @param array $where
     * @return Models
     */
    public function findWhereModels(array $where)
    {
        $this->applyCriteria();
        $this->applyConditions($where);

        $model = $this->model;
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model->whereIn($field, $values)->get($columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find data by excluding multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values, $columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model->whereNotIn($field, $values)->get($columns);
        $this->resetModel();

        return $model;
    }

    /**
     * Find whereIn model
     *
     * @param       $field
     * @param array $values
     *
     * @return Models
     */
    public function findWhereInModels($field, array $values, $onlyTrashed = false, $withTrashed = false)
    {
        $this->applyCriteria();
        if ($onlyTrashed)
            $model = $this->model->onlyTrashed();
        elseif ($withTrashed)
            $model = $this->model->withTrashed();
        $model = $this->model->whereIn($field, $values);
        $this->resetModel();

        return $model;
    }

    /**
     * Order collection by a given column
     *
     * @param string $column
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->model = $this->model->orderBy($column, $direction);

        return $this;
    }

    /**
     * Order collection by a given query
     *
     * @param string $query
     * @param string $direction
     *
     * @return $this
     */
    public function orderByRaw($query, $direction = 'asc')
    {
        $this->model = $this->model->orderByRaw($query . " " . $direction);

        return $this;
    }

    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes, $flushCache = true)
    {
        $model = $this->model->newInstance($attributes);
        $model->save();

        if ($flushCache) {
            logger('created called class = ' . get_class($this));
            event(new RepositoryEntityCreated(get_class($this), $model));
        }

        return $model;
    }

    /**
     * Save new entities in repository
     *
     * @param array $items
     * @param $flushCache
     * @return mixed
     */
    public function createMultiples(array $items, $flushCache = true)
    {
        $r = $this->model()::insert($items);
        if ($flushCache) {
            event(new RepositoryEntityCreated(get_class($this), null));
        }
        return $r;
    }

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     * @param bool $flushCache
     *create
     * @return mixed
     */
    public function update(array $attributes, $id, $flushCache = true)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($attributes);
        $model->save();

        if ($flushCache) {
            event(new RepositoryEntityUpdated(get_class($this), $model));
        }

        return $model;
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        try {
            $model = $this->find($id);
            $model->delete();
            event(new RepositoryEntityDeleted(get_class($this), $model));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return model
     */
    public function deleteAndResponseModel($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();

        return $model;
    }

    /**
     * Applies the given where conditions to the model.
     *
     * @param array $where
     * @return void
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $this->model = $this->model->where($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    /**
     * Load relations
     *
     * @param array|string $relations
     *
     * @return $this
     */
    public function with($relations)
    {
        $this->model = $this->model->with($relations);

        return $this;
    }

    /**
     * Apply criteria in current Query
     *
     * @return $this
     */
    protected function applyCriteria()
    {

        if ($this->skipCriteria === true) {
            return $this;
        }

        $criteria = $this->getCriteria();
        $model = $this->model;

        if ($criteria) {
            foreach ($criteria as $c) {
                if (is_array($c)) {
                    $this->model = $model->where(function ($model) use ($c) {
                        foreach ($c as $c1) {
                            $c1->apply($model, $this);
                        }
                        return $model;
                    });
                    continue;
                }

                if ($c instanceof CriteriaInterface) {
                    $this->model = $c->apply($this->model, $this);
                }
            }
        }

        return $this;
    }


    /**
     * Push Criteria for filter the query
     *
     * @param $criteria
     *
     * @return $this
     */
    public function pushCriteria($criteria)
    {
        if (is_string($criteria)) {
            $criteria = new $criteria;
        }
        if (!$criteria instanceof CriteriaInterface) {
            //throw new RepositoryException("Class " . get_class($criteria) . " must be an instance of Prettus\\Repository\\Contracts\\CriteriaInterface");
        }

        $this->criteria->push($criteria);

        return $this;
    }

    /**
     * Pop Criteria
     *
     * @param $criteria
     *
     * @return $this
     */
    public function popCriteria($criteria)
    {
        $this->criteria = $this->criteria->reject(function ($item) use ($criteria) {
            if (is_object($item) && is_string($criteria)) {
                return get_class($item) === $criteria;
            }

            if (is_string($item) && is_object($criteria)) {
                return $item === get_class($criteria);
            }

            return get_class($item) === get_class($criteria);
        });

        return $this;
    }

    /**
     * Get Collection of Criteria
     *
     * @return Collection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Find data by Criteria
     *
     * @param CriteriaInterface $criteria
     *
     * @return mixed
     */
    public function getByCriteria(CriteriaInterface $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);
        $results = $this->model->get();
        $this->resetModel();

        return $this->parserResult($results);
    }

    /**
     * Skip Criteria
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;

        return $this;
    }

    /**
     * Reset all Criterias
     *
     * @return $this
     */
    public function resetCriteria()
    {
        $this->criteria = new Collection();

        return $this;
    }

    public function resetModel()
    {
        $this->makeModel();
    }

    /**
     * Init instance
     * @param array $attributes
     * @return model
     */
    public function newInstance(array $attributes = [])
    {
        return $this->model->newInstance($attributes);
    }

    /**
     * Delete a entity in repository by conditions
     *
     * @param array $conditions
     * @return mixed
     */
    public function deleteByConditions(array $conditions)
    {
        event(new RepositoryEntityDeleted($this, null));
        return $this->model->where($conditions)->delete();
    }

    /**
     * Destroy multiple items by ID
     *
     * @param array $values
     *
     * @return mixed
     */
    public function forceDeleteMultipleByID(array $values)
    {
        return DB::table($this->model->getTable())
            ->whereIn('id', $values)
            ->delete();
    }

    /**
     * Find multi models by where IN
     *
     * @param       $field
     * @param array $values
     *
     * @return mixed
     */
    public function findMultiModelsByWhereIn($field, array $values)
    {
        $this->applyCriteria();
        $model = $this->model->whereIn($field, $values);
        $this->resetModel();

        return $model;
    }

    /**
     * Get first record with column
     *
     * @param array $columns
     * @return mixed
     */
    public function first($columns = ['*'])
    {
        $this->applyCriteria();
        $model = $this->model->first($columns);
        $this->resetModel();
        return $model;
    }

    /**
     * @param $slugOrId
     * @param $forceSlug
     * @param $idColumn
     * @param $slugColumn
     * @return mixed
     */
    public function findBySlugOrId($slugOrId, $forceSlug = false, $idColumn = 'id', $slugColumn = 'slug')
    {
        $this->resetCriteria();
        return $this->pushCriteria(new FilterByIdOrSlug($slugOrId, $idColumn, $slugColumn, $forceSlug))->first();
    }

    /**
     * Reset Query Scope
     *
     * @return $this
     */
    public function resetScope()
    {
        $this->scopeQuery = null;

        return $this;
    }

    /**
     * Apply scope in current Query
     *
     * @return $this
     */
    protected function applyScope()
    {
        if (isset($this->scopeQuery) && is_callable($this->scopeQuery)) {
            $callback = $this->scopeQuery;
            $this->model = $callback($this->model);
        }

        return $this;
    }

    /**
     * Query Scope
     *
     * @param \Closure $scope
     *
     * @return $this
     */
    public function scopeQuery(\Closure $scope)
    {
        $this->scopeQuery = $scope;

        return $this;
    }

    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function forceDelete($id)
    {
        $this->resetModel();
        return $this->model->find($id)->forceDelete();
    }

    /**
     * attempt to locate a database record using the given column / value pairs. If the model can not be found in the database,
     * a record will be inserted with the attributes from the first parameter, along with those in the optional second parameter
     *
     * array $arrayData
     * array $addData
     *
     * @return \Eloquent
     */
    public function firstOrCreate($arrayData, $addData = [])
    {
        $this->resetModel();
        return $this->model->firstOrCreate($arrayData, $addData);
    }

    /**
     * update or create
     * array $arrayData
     * array $addData
     *
     * @return \Eloquent
     */
    public function updateOrCreate($arrayData, $addData = [])
    {
        $this->resetModel();
        $model = $this->model->updateOrCreate($arrayData, $addData);
        event(new RepositoryEntityCreated(get_class($this), $model));
        return $model;
    }

    /**
     * like firstOrCreate will attempt to locate a record in the database matching the given attributes. However, if a model is not found,
     * a new model instance will be returned. Note that the model
     * returned by firstOrNew has not yet been persisted to the database. You will need to call save manually to persist it
     *
     * array $arrayData
     * array $addData
     *
     * @return $this->model
     */
    public function firstOrNew($arrayData, $addData = [])
    {
        $this->resetModel();
        return $this->model->firstOrNew($arrayData, $addData);
    }

    /**
     * Get Model
     * @return \Eloquent
     */
    public function getModel()
    {
        return $this->model;
    }
}
