<?php 
namespace Elidev\Repository\Traits;

use Illuminate\Support\Facades\DB;

trait QueryTrait
{
    /**
     * Items per page
     * @var integer $page
     */
    private $page = 1;

    /**
     * Limit response items
     * @var integer $limit
     */
    private $limit;

    /**
     * Which column you want to sort
     * @var string $sortColumn
     */
    private $sortColumn;

    /**
     * ASC, DESC
     * @var string $sortOrder
     */
    private $sortOrder;

    /**
     * Get data from a specific position
     * @var integer $offset
     */
    private $offset;

    /**
     * Extract paging, sort, limit data from request input
     *
     * @param array $params
     * @param array $defaultSort
     *
     * @return array
     */
    public function formatForListing($params = [], $defaultSort = [])
    {
        $allowedSortOrder = ['desc' => '-'];
        $pagination = [
            'limit'      => config('elidev-repository.pagination.limit'),
            'sortColumn' => 'id',
            'sortOrder'  => 'desc',
            'skip'       => 0,
            'page'       => 1,
            'offset'     => null,
            'count_only' => 0
        ];

        if (! empty($params['page'])) {
            $pagination['page'] = intval($params['page']);
        }

        if (isset($params['offset']) && ($params['offset'] != '')) {
            $pagination['offset'] = intval($params['offset']);
        }

        if (! empty($params['count_only'])) {
            $pagination['limit'] = 1;
        } elseif (isset($params['limit']) && intval($params['limit']) > 0) {
            $pagination['limit'] = intval($params['limit']);
        }

        if (isset($params['skip']) && intval($params['skip']) > 0) {
            $pagination['skip'] = intval($params['skip']);
        }

        /**
         * Handle sorting
         * $orderStr = -{field} such as -name or {field} such as name
         */
        if ( !empty($params['sort']) ) {
            $sortStr = $params['sort'];
        }
        else if (!empty($defaultSort)) {
            $sortStr = $defaultSort;
        }

        if (!empty($sortStr)) {
            $sortOrder = trim(is_string($sortStr) ? substr($sortStr, 0, 1) : '');
            $pagination['sortOrder'] = in_array($sortOrder, $allowedSortOrder) ? 'desc' : 'asc';

            $pagination['sortColumn'] = in_array($sortOrder , $allowedSortOrder) ? substr($sortStr, 1, strlen($sortStr) - 1) : $sortStr;
        }
        /**
         * END Handle sorting
         */

        $this->setOffset($pagination['offset']);
        $this->setPage($pagination['page']);
        $this->setLimit($pagination['limit']);
        $this->setSortColumn($pagination['sortColumn']);
        $this->setSortOrder($pagination['sortOrder']);

        return $pagination;
    }

    /**
     * @param array $columns
     * @return string
     */
    public function selectWithFoundRows($columns = []) {
        $selectClause = implode(',', $columns);
        return ' SQL_CALC_FOUND_ROWS '. $selectClause;
    }

    /**
     * Get total rows of the previous query
     * @return integer
    */
    private function getTotal() {
        return (int) DB::select("SELECT FOUND_ROWS() as `row_count`")[0]->row_count;
    }

    /**
     * @param $result
     * @return array $result
     */
    public function result($result) {
        $result->total = $this->getTotal();
        $result->limit = $this->getLimit();
        $result->page = $this->getPage();

        $result->lastPage = ($this->getTotal() - $this->getTotal() % $this->getLimit())/$this->getLimit() + 1;

        return $result;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getSortColumn()
    {
        return $this->sortColumn;
    }

    /**
     * @param mixed $sortColumn
     */
    public function setSortColumn($sortColumn)
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;

    }
}