<?php

namespace Elidev\Repository\Traits;

use App\Helper\FomoHelper;

trait SEOInfoTrait {
    /**
     * Handle seo info
     *
     * @param        $title
     * @param        $description
     * @param        $type
     * @param        $image
     * @param string $tags
     * @param string $publishedTime
     * @return array[]
     */
    public function handleSeoInfo($title, $description, $type, $image, $tags = '', $publishedTime = '')
    {
        return [
            'seo_info' => [
                'type'           => $type,
                'site_name'      => FomoHelper::getSiteName(),
                'title'          => $title,
                'description'    => $description,
                'image'          => $image,
                'published_time' => $publishedTime,
                'tags'           => $tags
            ]
        ];
    }
}