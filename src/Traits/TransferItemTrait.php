<?php

namespace Elidev\Repository\Traits;

use Elidev\Repository\Eloquent\TransferItems;

trait TransferItemTrait
{

    /**
     * @param $originalID - ID of item from the third party
     * @return int
     */
    public function getTargetId($originalID)
    {
        $data = TransferItems::where([
            'type' => $this->setTransferDataType(),
            'original_id' => $originalID
        ])->first();

        if (!$data) return 0;

        $target = $this->find($data->target_id);
        if (empty($target)) {
            TransferItems::where([
                'id'          => $data->id,
                'original_id' => $originalID,
                'target_id'   => $data->target_id,
                'type'        => $this->setTransferDataType()
            ])->delete();
        }

        if (!$target) return 0;

        return $target->id;
    }

    /**
     * Store transfer item
     * @param $originalID - ID of item from the third party
     * @param $targetId - ID on the Fomo system
     * @return mixed
     */
    public function storeTransferItem($originalID, $targetId = 0) {

        if (!$targetId) {
            $targetId = isset($this->id) ? $this->id : 0;
        }

        if (!$targetId) {
            return false;
        }

        return TransferItems::create([
            'original_id' => $originalID,
            'target_id' => $targetId,
            'type' => $this->setTransferDataType()
        ]);
    }

    /**
     * Set transfer type
     * @return string
     */
    abstract public function setTransferDataType();


}