<?php

namespace Elidev\Repository\Traits;

use Illuminate\Contracts\Cache\Repository as CacheRepository;

use Elidev\Repository\Contracts\CriteriaInterface;

use Elidev\Repository\Helpers\CacheKeys;
use ReflectionObject;
use Exception;
use Cache;

/**
 * Class CacheableRepository
 * @package Elidev\Repository\Traits
 */
trait CacheableRepository
{

    use ComparesVersionsTrait;

    /**
     * @var CacheRepository
     */
    protected $cacheRepository = null;

    /**
     * Set Cache Repository
     *
     * @param CacheRepository $repository
     *
     * @return $this
     */
    public function setCacheRepository(CacheRepository $repository)
    {
        $this->cacheRepository = $repository;

        return $this;
    }

    /**
     * Skip Cache
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipCache($status = true)
    {
        $this->cacheSkip = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSkippedCache()
    {
        if (request()->has('withoutCache')) { // Skip cache
            return true;
        }

        $skipped = isset($this->cacheSkip) ? $this->cacheSkip : false;
        $request = app('Illuminate\Http\Request');
        $skipCacheParam = config('elidev-repository.cache.params.skipCache', 'skipCache');

        if ($request->has($skipCacheParam) && $request->get($skipCacheParam)) {
            $skipped = true;
        }

        return $skipped;
    }

    /**
     * @param $method
     *
     * @return bool
     */
    protected function allowedCache($method)
    {
        $cacheEnabled = config('elidev-repository.cache.enabled', true);

        if (!$cacheEnabled) {
            return false;
        }

        $cacheOnly = isset($this->cacheOnly) ? $this->cacheOnly : config('elidev-repository.cache.allowed.only', null);
        $cacheExcept = isset($this->cacheExcept) ? $this->cacheExcept : config('elidev-repository.cache.allowed.except', null);

        if (is_array($cacheOnly)) {
            return in_array($method, $cacheOnly);
        }

        if (is_array($cacheExcept)) {
            return !in_array($method, $cacheExcept);
        }

        if (is_null($cacheOnly) && is_null($cacheExcept)) {
            return true;
        }

        return false;
    }

    /**
     * Get Cache key for the method
     *
     * @param $method
     * @param $args
     * @param bool $hashKey
     * @return string
     */
    public function getCacheKey($method, $args = null, $hashKey = true)
    {
        $request = app('Illuminate\Http\Request');
        if ($hashKey) {
            $args = serialize($args);
        }
        $criteria = $this->serializeCriteria();

        if ($hashKey) {
            $key = sprintf('%s@%s-%s', get_called_class(), $method, md5($args . $criteria . $request->fullUrl()));
        } else {
            $key = sprintf('%s@%s-%s', get_called_class(), $method, $args);
        }

        CacheKeys::putKey(get_called_class(), $key);

        return $key;
    }

    /**
     * Get Cache key for the single record e.g find, findOrFail
     *
     * @param $method
     * @return string
     */
    public function getCacheKeyForSingleRecord($method, $id)
    {
        $class = get_called_class();
        $key = sprintf('%s@%s---@%s', $class, $method, $id);
        CacheKeys::putKey($class, $key);

        return $key;
    }

    /**
     * Serialize the criteria making sure the Closures are taken care of.
     *
     * @return string
     */
    protected function serializeCriteria()
    {
        try {
            return serialize($this->getCriteria());
        } catch (Exception $e) {
            return serialize($this->getCriteria()->map(function ($criterion) {
                return $this->serializeCriterion($criterion);
            }));
        }
    }

    /**
     * Serialize single criterion with customized serialization of Closures.
     *
     * @param  \Prettus\Repository\Contracts\CriteriaInterface $criterion
     * @return \Prettus\Repository\Contracts\CriteriaInterface|array
     *
     * @throws \Exception
     */
    protected function serializeCriterion($criterion)
    {
        try {
            serialize($criterion);

            return $criterion;
        } catch (Exception $e) {
            // We want to take care of the closure serialization errors,
            // other than that we will simply re-throw the exception.
            if ($e->getMessage() !== "Serialization of 'Closure' is not allowed") {
                throw $e;
            }

            $r = new ReflectionObject($criterion);

            return [
                'hash' => md5((string) $r),
            ];
        }
    }

    /**
     * Get cache time
     *
     * Return minutes: version < 5.8
     * Return seconds: version >= 5.8
     *
     * @return int
     */
    public function getCacheTime()
    {
        $cacheMinutes = isset($this->cacheMinutes) ? $this->cacheMinutes : config('elidev-repository.cache.minutes', 240);

        /**
         * https://laravel.com/docs/5.8/upgrade#cache-ttl-in-seconds
         */
        if ($this->versionCompare($this->app->version(), "5.7.*", ">")) {
            return (int) $cacheMinutes * 60;
        }

        return (int) $cacheMinutes;
    }

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null  $limit
     * @param array $columns
     * @param string $method
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = 'paginate')
    {
        if (!$this->allowedCache('paginate') || $this->isSkippedCache()) {
            return parent::paginate($limit, $columns, $method);
        }
        $key = $this->getCacheKey('paginate', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::paginate($limit, $columns, $method);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }
        $this->resetModel();
        $this->resetScope();

        return $value;
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     * @param bool $withoutCache
     *
     * @return mixed
     */
    public function find($id, $columns = ['*'], $withoutCache = false)
    {
        if ($withoutCache || !$this->allowedCache('find') || $this->isSkippedCache()) {
            return parent::find($id, $columns);
        }

        $key = $this->getCacheKeyForSingleRecord('find', $id);
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->singleObjectCacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::find($id, $columns);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();

        return $value;
    }

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value = null, $columns = ['*'])
    {
        if (!$this->allowedCache('findByField') || $this->isSkippedCache()) {
            return parent::findByField($field, $value, $columns);
        }

        if (is_array($value)) {
            $key = $this->getCacheKey('findByField', func_get_args());
        } else {
            $key = $this->getCacheKeyForSingleRecord('findByField', 'single:'. $field.':'. $value);
        }

        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::findByField($field, $value, $columns);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*'])
    {
        if (!$this->allowedCache('findWhere') || $this->isSkippedCache()) {
            return parent::findWhere($where, $columns);
        }

        $key = $this->getCacheKey('findWhere', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::findWhere($where, $columns);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Find data by Criteria
     *
     * @param CriteriaInterface $criteria
     *
     * @return mixed
     */
    public function getByCriteria(CriteriaInterface $criteria)
    {
        if (!$this->allowedCache('getByCriteria') || $this->isSkippedCache()) {
            return parent::getByCriteria($criteria);
        }

        $key = $this->getCacheKey('getByCriteria', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::getByCriteria($criteria);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Get results in limitation with page and offset
     * @param array $columns
     * @param null $limit
     * @param int $offset
     * @param int $page
     * @return \Eloquent
     */
    public function getLimitWithPageOffset($columns = ['*'], $limit = null, $offset = 0, $page = 1){
        if (!$this->allowedCache('getLimitWithPageOffset') || $this->isSkippedCache()) {
            return parent::getLimitWithPageOffset($columns, $limit, $offset, $page);
        }

        $key = $this->getCacheKey('getLimitWithPageOffset', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::getLimitWithPageOffset($columns, $limit, $offset, $page);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        if (!$this->allowedCache('all') || $this->isSkippedCache()) {
            return parent::all($columns);
        }

        $key = $this->getCacheKey('all', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::all($columns);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Get first record with column
     *
     * @param array $columns
     * @return mixed
     */
    public function first($columns = ['*'])
    {
        if (!$this->allowedCache('first') || $this->isSkippedCache()) {
            return parent::first($columns);
        }

        $key = $this->getCacheKey('first', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::first($columns);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }

        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * Count where
     *
     * @param array $where
     *
     * @return integer
     */
    public function countWhere(array $where)
    {
        if (!$this->allowedCache('countWhere') || $this->isSkippedCache()) {
            return parent::countWhere($where);
        }

        $key = $this->getCacheKey('countWhere', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::countWhere($where);
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();

        return $value;
    }


    /**
     * Count
     *
     * @return integer
     */
    public function count()
    {
        if (!$this->allowedCache('count') || $this->isSkippedCache()) {
            return parent::count();
        }

        $key = $this->getCacheKey('count', func_get_args());
        $seconds = $this->getCacheTime();

        if (!Cache::has($key) || $this->cacheIsOutOfDate($key, $seconds)) {
            $this->setExpiredCacheTime($key, $seconds);
            $value = parent::count();
            if ($value !== null) {
                Cache::put($key, $value, $seconds);
            }
        } else {
            $value = Cache::get($key);
        }

        $this->resetModel();
        $this->resetScope();

        return $value;
    }

    /**
     * Store expired cache time for a cache key
     * @param string $key
     * @param int $seconds
     * @return void
     */
    private function setExpiredCacheTime(string $key, int $seconds)
    {
        Cache::forever(CacheKeys::getExpiredCacheTimeKey($key), now()->addSeconds($seconds));
    }

    /**
     * Check whether a cache was out of date or not
     * When new record is coming, storing the refreshed time of a group cache in RepositoryEventBase
     * that aims to force fetching data from the database instead of cache.
     * Just in case, the clearing cache queue job is still in progress, cache hasn't been refreshed yet. In this case,
     * we need to fetch data from the database instead of pulling old cache data
     *
     * @param string $key
     * @param int $seconds
     * @return bool
     */
    private function cacheIsOutOfDate(string $key, int $seconds): bool
    {
        try {
            $expiredCacheTime = Cache::get(CacheKeys::getExpiredCacheTimeKey($key)); // Get expired cache time of a key
            $refreshedCacheTimeOfGroupCache = Cache::get(CacheKeys::getRefreshedCacheTimeKey(get_called_class())); // Get expired cache time of a container

            if (!$expiredCacheTime || !$refreshedCacheTimeOfGroupCache) {
                return false;
            }

            // Is the time when cache was refreshed plus the life time of the cache
            $refreshedCacheTimePlusTTL = $refreshedCacheTimeOfGroupCache->addSeconds($seconds);
            $r = $refreshedCacheTimePlusTTL->gt($expiredCacheTime); // Cache is out of date
            if ($r) {
                logger('cache was out of date key = '. $key);
            }
            return $r;
        } catch (Exception $e) {
            logger($e->getMessage());
            return true;
        }
    }

    /**
     * Check whether a single object cache was out of date or not
     *
     * When new record is coming, storing the refreshed time of a group cache in RepositoryEventBase
     * that aims to force fetching data from the database instead of cache.
     * Just in case, the clearing cache queue job is still in progress, cache hasn't been refreshed yet. In this case,
     * we need to fetch data from the database instead of pulling old cache data
     *
     * @param string $key
     * @param int $seconds
     * @return bool
     */
    private function singleObjectCacheIsOutOfDate(string $key, int $seconds): bool
    {
        try {
            $expiredCacheTime = Cache::get(CacheKeys::getExpiredCacheTimeKey($key)); // Get expired cache time of a key
            $refreshedCacheTime = Cache::get(CacheKeys::getRefreshedCacheTimeKey($key)); // Get expired cache time of a container

            if (!$expiredCacheTime || !$refreshedCacheTime) {
                return false;
            }

            // Is the time when cache was refreshed plus the life time of the cache
            $refreshedCacheTimePlusTTL = $refreshedCacheTime->addSeconds($seconds);

            $r = $refreshedCacheTimePlusTTL->gt($expiredCacheTime); // Cache is out of date
            if ($r) {
                logger('singleObjectCacheIsOutOfDate was out of date key = '. $key);
            }
            return $r;
        } catch (Exception $e) {
            logger($e->getMessage());
            return true;
        }
    }
}
