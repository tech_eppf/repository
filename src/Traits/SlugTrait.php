<?php

namespace Elidev\Repository\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

trait SlugTrait
{
    /**
     * Generate Slug and update to database
     *
     * @param string $columnPrimaryGenerate
     * @param string | array | null $columnSecondGenerate
     */
    public function generateSlug()
    {
        /** @var Model $this */

        $generateFrom = $this->generateSlugFrom();
        $nameGenerate = null;
        if (is_array($generateFrom)) {
            $columnPrimaryGenerate = isset($generateFrom['primary']) ? $generateFrom['primary'] : null;
            $columnSecondGenerate = isset($generateFrom['second']) ? $generateFrom['second'] : null;

            $nameGenerate = $columnPrimaryGenerate ? $this->{$columnPrimaryGenerate} : null;

            if (!$nameGenerate && $columnSecondGenerate) {
                if (is_array($columnSecondGenerate)) {
                    foreach ($columnSecondGenerate as $item) {
                        if ( is_null($nameGenerate) ) {
                            $nameGenerate = $this->{$item} ?? null;
                        }else {
                            $nameGenerate .= '-' . $this->{$item} ?? null;
                        }
                    }
                }else {
                    $nameGenerate = $this->{$columnSecondGenerate};
                }
            }
        }else {
            $nameGenerate = $this->{$generateFrom} ?? null;
        }

        if ($nameGenerate) {
            $slug = str_slug($nameGenerate);
            $existSlug = DB::table($this->table)->where('slug', $slug)->first();
            if ($existSlug && $existSlug->id != $this->id) {
                $slug .= '-' . $this->id;
            }
            DB::table($this->table)->where(['id' => $this->id])->update(['slug' => $slug]);
        }
    }

    /**
     * @description get column field to generate slug
     *
     * @return string columns or array [
     *      'primary' => columnPrimaryGenerate,
     *      'second' => string columnSecondGenerate or array [ 'column1', 'column2' ...]
     * ]
     */
    abstract public function generateSlugFrom();
}