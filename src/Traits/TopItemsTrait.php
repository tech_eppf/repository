<?php

namespace Elidev\Repository\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

trait TopItemsTrait
{
    /**
     * Remove expired top items (events, courses)
     * @param $date
     * @return mixed
     */
    public function removeExpiredTopItems($date) {
        return $this->model->where('top', 1)->whereDate('top_expiry_date', '<', $date)->update([
            'top' => 0,
            'top_expiry_date' => null
        ]);
    }
}