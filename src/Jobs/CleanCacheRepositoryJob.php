<?php

namespace Elidev\Repository\Jobs;

use App\Repositories\Contracts\AHRepositoryInterface;
use App\Services\WpPushNews\Admin\PushFactory;
use Elidev\Repository\Helpers\CacheRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Illuminate\Support\Arr;
use Modules\MultiClient\Helpers\MultiHelper;

class CleanCacheRepositoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     * PushNewsJob constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data =  $data;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        CacheRepository::clear($this->data);
    }

    public function failed(Exception $exception)
    {

    }

}
