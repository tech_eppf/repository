<?php 
namespace Elidev\Repository\Contracts;

/**
 * Interface CriteriaInterface
 * @package Elidev\Repository\Contracts
 */
interface CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository);
}
