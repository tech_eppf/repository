<?php
namespace Elidev\Repository\Contracts;

interface RepositoryInterface
{
    /**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*']);

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*']);

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*']);

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     * @param bool $withoutCache
     * @return mixed
     */
    public function find($id, $columns = ['*'], $withoutCache = false);

    /**
     * Find data by id including soft delete data
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findIncludeSoftDelete($id, $columns = ['*']);

    /**
     * Find soft deleted data by id
     *
     * @param integer $id
     * @param array $columns
     *
     * @return boolean
     */
    public function findSoftDelete($id, $columns = ['*']);

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findOrFail($id, $columns = ['*']);

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*']);

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*']);

    /**
     * Find data by multiple fields without cache
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereWithoutCache(array $where, $columns = ['*']);

    /**
     * Find by ids
     *
     * @param array $ids
     *
     * @return mixed
     */
    public function findByIds(array $ids);

    /**
     * Count where
     *
     * @param array $where
     *
     * @return integer
     */
    public function countWhere(array $where);

    /**
     * Count
     *
     * @return integer
     */
    public function count();

    /**
     * Find wheres model
     *
     * @param array $where
     * @return Models
     */
    public function findWhereModels(array $where);

    /**
     * Find data by multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*']);

    /**
     * Find data by excluding multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values, $columns = ['*']);

    /**
     * Order collection by a given column
     *
     * @param string $column
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'asc');

    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes, $flushCache = true);

    /**
     * Save new entities in repository
     *
     * @param array $items
     * @param $flushCache
     * @return mixed
     */
    public function createMultiples(array $items, $flushCache = true);

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     * @param bool $flushCache
     *create
     * @return mixed
     */
    public function update(array $attributes, $id, $flushCache = true);

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id);

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return model
     */
    public function deleteAndResponseModel($id);

    /**
     * Init instance
     * @param array $attributes
     * @return model
     */
    public function newInstance(array $attributes = []);

    /**
     * Load relations
     *
     * @param $relations
     *
     * @return $this
     */
    public function with($relations);

    /**
     * Delete a entity in repository by conditions
     *
     * @param array $conditions
     * @return mixed
     */
    public function deleteByConditions(array $conditions);

    /**
     * Destroy multiple items by ID
     *
     * @param array $values
     *
     * @return mixed
     */
    public function forceDeleteMultipleByID(array $values);

    /**
     * Find multi models by where IN
     *
     * @param       $field
     * @param array $values
     *
     * @return mixed
     */
    public function findMultiModelsByWhereIn($field, array $values);

    /**
     * Get results in limitation with page and offset
     * @param array $columns
     * @param null $limit
     * @param int $offset
     * @param int $page
     * @return \Eloquent
     */
    public function getLimitWithPageOffset($columns = ['*'], $limit = null, $offset = 0, $page = 1);

    /**
     * Get first record with column
     *
     * @param array $columns
     * @return mixed
     */
    public function first($columns = ['*']);

    /**
     * Get first record by param slug or id
     *
     * @param int | string $slugOrId
     * @param $forceSlug
     * @param $idColumn
     * @param $slugColumn
     * @return mixed
     */
    public function findBySlugOrId($slugOrId, $forceSlug = false, $idColumn = 'id', $slugColumn = 'slug');

    /**
     * Query Scope
     *
     * @param \Closure $scope
     *
     * @return $this
     */
    public function scopeQuery(\Closure $scope);

    /**
     * Reset Query Scope
     *
     * @return $this
     */
    public function resetScope();

    /**
     * @param $id
     * @return mixed
     */
    public function forceDelete($id);

    /**
     * attempt to locate a database record using the given column / value pairs. If the model can not be found in the database,
     * a record will be inserted with the attributes from the first parameter, along with those in the optional second parameter
     *
     * array $arrayData
     * array $addData
     *
     * @return \Eloquent
     */
    public function firstOrCreate($arrayData, $addData = []);

    /**
     * update or create
     * array $arrayData
     * array $addData
     *
     * @return \Eloquent
     */
    public function updateOrCreate($arrayData, $addData = []);

    /**
     * like firstOrCreate will attempt to locate a record in the database matching the given attributes. However, if a model is not found,
     * a new model instance will be returned. Note that the model
     * returned by firstOrNew has not yet been persisted to the database. You will need to call save manually to persist it
     *
     * array $arrayData
     * array $addData
     *
     * @return $this->model
     */
    public function firstOrNew($arrayData, $addData = []);
}
