<?php

namespace Elidev\Repository\Listeners;

use Elidev\Repository\Helpers\CacheKeys;
use Elidev\Repository\Helpers\CacheRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Modules\MultiClient\Helpers\MultiHelper;

/**
 * Class CleanCacheRepository
 * @package Elidev\Repository\Listeners
 */
class CleanCacheRepository implements ShouldQueue
{
    public function handle($event)
    {
        $repositoryName = $event->getRepository();
        $repository = app($repositoryName);
        $checkFunctionExists = method_exists($repository, 'getCacheOnlyVariable');
        $turnOnCacheOfModel = $checkFunctionExists ? $repository->getCacheOnlyVariable() : false;
        $cleanEnabled = config("elidev-repository.cache.clean.enabled", false);
        $enableRepositoryCache = config("elidev-repository.cache.enabled", false);


        // If cache is enable and model repository turn on cache via cacheOnly variable
        if ($enableRepositoryCache && $cleanEnabled && !empty($turnOnCacheOfModel)) {
            $repoStr = get_class($repository);
            $cacheKeys = CacheKeys::getKeysForCleaningCache($repoStr);
            $action = $event->getAction();
            $model = $event->getModel();
            $data = [
                'cache_key' => $cacheKeys,
                'action'    => $action,
                'model'     => [
                    'slug' => isset($model->slug) ? $model->slug : '',
                    'id'   => isset($model->id) ? $model->id : ''
                ]
            ];

            CacheRepository::clear($data, $repoStr);
        }
    }
}
