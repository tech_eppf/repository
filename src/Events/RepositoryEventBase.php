<?php
namespace Elidev\Repository\Events;

use Elidev\Repository\Helpers\CacheKeys;
use Illuminate\Database\Eloquent\Model;
use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Class RepositoryEventBase
 * @package Elidev\Repository\Events
 */
abstract class RepositoryEventBase
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var string
     */
    protected $action;

    /**
     * @param $repository
     * @param Model               $model
     */
    public function __construct($repository, $model = null)
    {
        /**
         * When new record is coming, storing the refreshed time of a group cache
         * that aims to force fetching data from the database instead of cache.
         * Just in case, the clearing cache queue job is still in progress, cache hasn't been refreshed yet. In this case,
         * we need to fetch data from the database instead of pulling old cache data
         */
        $group = is_object($repository) ? get_class($repository):$repository;
        logger('RepositoryEventBase / setRefreshedCacheTimeGroupKey = '. $group);
        CacheKeys::setRefreshedCacheTimeGroupKey($group);

        $this->repository = $repository;
        $this->model = $model;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return RepositoryInterface
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}
