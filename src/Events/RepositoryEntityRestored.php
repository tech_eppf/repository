<?php
namespace Elidev\Repository\Events;

/**
 * Class RepositoryEntityRestored
 *
 * @package Elidev\Repository\Events
 */
class RepositoryEntityRestored extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = 'restored';
}
