<?php
namespace Elidev\Repository\Events;

/**
 * Class RepositoryEntityCreated
 * @package Elidev\Repository\Events
 */
class RepositoryEntityCreated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "created";
}
