<?php
namespace Elidev\Repository\Events;

/**
 * Class RepositoryEntityUpdated
 * @package Elidev\Repository\Events
 */
class RepositoryEntityUpdated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "updated";
}
