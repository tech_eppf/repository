<?php

namespace Elidev\Repository\Helpers;
use Cache;
use Illuminate\Support\Arr;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class CacheKeys
 * @package Elidev\Repository\Helpers
 */
class CacheKeys
{

    /**
     * @var string
     */
    protected static $storeFile = "repository-cache-keys.json";

    /**
     * @var array
     */
    protected static $keys = null;

    /**
     * @param $group
     * @param $key
     *
     * @return void
     */
    public static function putKey($group, $key)
    {
        self::loadKeys();

        self::$keys[$group] = self::getKeys($group);

        if (!in_array($key, self::$keys[$group])) {
            self::$keys[$group][] = $key;
        }

        self::storeKeys();
    }

    /**
     * @return array|mixed
     */
    public static function loadKeys()
    {
        if (!is_null(self::$keys) && is_array(self::$keys)) {
            return self::$keys;
        }

        $file = self::getFileKeys();

        if (!Cache::has($file)) {
            self::storeKeys();
        }

        self::$keys = Cache::get($file);
        return self::$keys;
    }

    /**
     * Get key from cache key container for cleaning cache instead of fetching it
     * from the static variable
     * @return array
     */
    public static function getKeysForCleaningCache($group): array
    {
        $file = self::getFileKeys();
        $cacheKeys = Cache::get($file);
        $groupCacheKeys = Arr::get($cacheKeys, $group);
        if (is_array($groupCacheKeys)) {
            return $groupCacheKeys;
        }
        return [];
    }

    /**
     * @return string
     */
    public static function getFileKeys()
    {
        $filename = config('elidev-repository.cache.cache_key_file'); // Can override filename for multiple sites system

        if (!$filename) {
            $filename = self::$storeFile;
        }

        return $filename;
    }

    /**
     * Renamed the file key
     * @return void
    */
    public static function renamedFileKey($prefix): void
    {
        config(['elidev-repository.cache.cache_key_file' => sprintf('org:%s:repository-cache-keys.json', $prefix)]);
    }

    /**
     * @return int
     */
    public static function storeKeys()
    {
        $file = self::getFileKeys();
        self::$keys = is_null(self::$keys) ? [] : self::$keys;
        Cache::forever($file, self::$keys);
    }

    /**
     * @param $group
     *
     * @return array|mixed
     */
    public static function getKeys($group)
    {
        self::loadKeys();
        self::$keys[$group] = isset(self::$keys[$group]) ? self::$keys[$group] : [];

        return self::$keys[$group];
    }

    /**
     * @param $method
     * @param $parameters
     *
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        $instance = new static;

        return call_user_func_array([
            $instance,
            $method
        ], $parameters);
    }

    /**
     * @param $method
     * @param $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $instance = new static;

        return call_user_func_array([
            $instance,
            $method
        ], $parameters);
    }

    /**
     * Generate expired cache time key
     * @string $key
     * @param $key
     * @return string
     */
    public static function getExpiredCacheTimeKey($key): string
    {
        return $key.'_expired_cache_time';
    }

    /**
     * Generate refreshed cache time key
     * @string $key
     * @param $group
     * @return string
     */
    public static function getRefreshedCacheTimeKey($group): string
    {
        return $group.'_refreshed_cache_time';
    }

    /**
     * Set refreshed cache time key of a repository group
     * @string $key
     * @param $group
     * @return void
     */
    public static function setRefreshedCacheTimeGroupKey($group)
    {
        Cache::forever(self::getRefreshedCacheTimeKey($group), now());
    }

    /**
     * Set refreshed cache time key of a single object key e.g the result of the find function
     * @string $key
     * @param $key
     * @return void
     */
    public static function setRefreshedCacheTimeSingleObject($key)
    {
        logger('set refresh cache key SINGLE OBJECT = '. $key);
        Cache::forever(self::getRefreshedCacheTimeKey($key), now());
    }
}
