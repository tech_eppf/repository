<?php

namespace Elidev\Repository\Helpers;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Arr;
use Cache;

class CacheRepository
{
    /**
     * @param array $data
     * @return void
     */
    public static function clear(array $data = [], $repository)
    {
        $cacheKeys = Arr::get($data, 'cache_key', []);
        $model = Arr::get($data, 'model', []);
        $action = Arr::get($data, 'action');

        if (config("elidev-repository.cache.clean.on.{$action}", true)) {
            if (is_array($cacheKeys)) {
                $modelId = Arr::get($model, 'id');

                foreach ($cacheKeys as $key) {
                    /**
                     * Only remove model cache of a specific ID, do not remove the other models
                     */
                    if ($modelId && strrpos($key, "@find---") !== false) {
                        if (strrpos($key, '---@'.$modelId) === false) { // Don't deleting cache of another model
                            continue;
                        } else {
                            // Store the refreshed time of single object cache for the find function cache checking
                            CacheKeys::setRefreshedCacheTimeSingleObject($key);
                        }

                    }
                    Cache::forget($key);
                }
            }

            // Store the refreshed time of the group cache
            logger('CacheRepository:clear: setRefreshedCacheTimeGroupKey = '. $repository);
            CacheKeys::setRefreshedCacheTimeGroupKey($repository);
        }
    }
}
