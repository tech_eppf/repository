<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;

class FilterByTop implements CriteriaInterface
{
    /**
     * @var bool
     */
    protected $top;

    /**
     * @var
     */
    protected $topColumn;

    /**
     * @var
     */
    protected $expiryDateColumn;

    /**
     * FilterByTop constructor.
     *
     * @param int    $top
     * @param string $topColumn
     * @param string $expiryDateColumn
     */
    public function __construct($top = -1, $topColumn = 'top', $expiryDateColumn = 'top_expiry_date')
    {
        $this->top = $top;
        $this->topColumn = $topColumn;
        $this->expiryDateColumn = $expiryDateColumn;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (($this->top == -1)) {
            return $model;
        }

        return $model
            ->where($this->topColumn, $this->top)
            ->where(function($query) {
                $currentDate = Carbon::now()->format('Y-m-d');
                $query
                    ->whereDate($this->expiryDateColumn, '>=', $currentDate)
                    ->orWhereNull($this->expiryDateColumn);
            });
    }
}
