<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByDoesHave implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $relatedTable;

    /**
     * FilterByDoesHave constructor.
     * @param $relatedTable
     */
    public function __construct($relatedTable)
    {
        $this->relatedTable = $relatedTable;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->relatedTable)) {
            return $model;
        }

        return $model->doesntHave($this->relatedTable);
    }
}
