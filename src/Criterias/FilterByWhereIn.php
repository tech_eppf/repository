<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByWhereIn implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * FilterByWhereIn constructor.
     *
     * @param $value
     * @param $fieldName
     */
    public function __construct($value, $fieldName)
    {
        $this->value = $value;
        $this->fieldName = $fieldName;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->value) || (! is_array($this->value)) || empty($this->fieldName)) {
            return $model;
        }

        return $model->whereIn($this->fieldName, $this->value);
    }
}
