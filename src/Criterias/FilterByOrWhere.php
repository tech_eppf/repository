<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByOrWhere implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $values;

    /**
     * FilterByOrWhere constructor.
     *
     * @param $values
     */
    public function __construct($values)
    {
        $this->values = $values;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->values)) {
            return $model;
        }

        $values = $this->values;

        return $model->where(function($query) use ($values) {
            foreach ($values as $key => $data) {
                $condition = '=';
                $fieldName = array_key_first($data);
                $value = $data[$fieldName];
                if (is_array($value)) {
                    $condition = $value['condition'];
                    $value = $value['value'];
                }

                if ($key == 0) {
                    $query->where($fieldName, $condition, $value);
                } else {
                    $query->orWhere($fieldName, $condition, $value);
                }
            }
        });
    }
}
