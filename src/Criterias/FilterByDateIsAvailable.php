<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;

class FilterByDateIsAvailable implements CriteriaInterface
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * FilterByAvailableDateTime constructor.
     * @param string $fieldName
     */
    public function __construct($fieldName = 'available_date')
    {
        $this->fieldName = $fieldName;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->fieldName)) {
            return $model;
        }

        return $model->where(function($query) {
            $query->where($this->fieldName, '<=', Carbon::now()->toDateTimeString())
                ->orWhereNull($this->fieldName)
                ->orWhere($this->fieldName, '');
        });
    }
}
