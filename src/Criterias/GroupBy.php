<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class GroupBy implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $fields;

    /**
     * GroupBy constructor.
     * @param array $fields
     */
    public function __construct($fields = [])
    {
        $this->fields = $fields;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->fields)) {
            return $model;
        }

        return $model->groupBy(implode(',', $this->fields));
    }
}
