<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByConcatField implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $firstColumn;

    /**
     * @var string
     */
    protected $secondColumn;

    /**
     * FilterByConcatField constructor.
     *
     * @param        $value
     * @param string $firstColumn
     * @param string $secondColumn
     */
    public function __construct($value, $firstColumn = '', $secondColumn = '')
    {
        $this->value = $value;
        $this->firstColumn = $firstColumn;
        $this->secondColumn = $secondColumn;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->value) || empty($this->firstColumn) || empty($this->secondColumn)) {
            return $model;
        }

        $value = $this->value;
        $firstColumn = $this->firstColumn;
        $secondColumn = $this->secondColumn;

        return $model->where(function ($query) use ($value, $firstColumn, $secondColumn) {
            $query->whereRaw("concat({$firstColumn}, ' ', {$secondColumn}) like '%{$value}%'")
                ->orWhereRaw("concat({$secondColumn}, ' ', {$firstColumn}) like '%{$value}%'");
        });
    }
}
