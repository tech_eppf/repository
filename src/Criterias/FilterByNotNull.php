<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByNotNull implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $field;

    protected $value;

    /**
     * FilterByNull constructor.
     *
     * @param string $field
     */
    public function __construct($field = '', $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->value) || empty($this->field)) {
            return $model;
        }
        return $model->whereNotNull($this->field);
    }
}
