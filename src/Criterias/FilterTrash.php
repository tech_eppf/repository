<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterTrash implements CriteriaInterface
{
    /**
     * @var bool
     */
    protected $trash;

    /**
     * FilterTrashCriteria constructor.
     *
     * @param integer $trash
     *
     * Note:
     *  - trash = 1 : get only users who have trashedOrderByRawCriteria.php
     *  - trash = 0 : get all active users who have not trashed
     */
    public function __construct($trash = 0)
    {
        $this->trash = $trash;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->trash ) {
            return $model->onlyTrashed();
        }

        return $model;
    }
}
