<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByBetweenDate implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $value;

    /**
     * MongoFilterBetweenDate constructor.
     * @param Carbon $value
     * @param $filedName
     */
    public function __construct($value, $filedName = '')
    {
        $this->value = $value;
        $this->fieldName = $filedName;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->fieldName) || empty($this->value)) {
            return $model;
        }
        return $model
            ->where($this->fieldName, '<=', $this->value->copy()->addDay()->toDateTime())
            ->where($this->fieldName, '>=', $this->value->toDateTime());
    }
}
