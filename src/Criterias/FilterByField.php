<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByField implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $compare;

    /**
     * FilterByFieldString constructor.
     * @param string $value
     * @param string$fieldName
     */
    public function __construct($value, $fieldName, $compare = '')
    {
        $this->value = $value;
        $this->fieldName = $fieldName;
        $this->compare = $compare;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->value) || empty($this->fieldName)) {
            return $model;
        }

        if ($this->compare) {
            return $model->where($this->fieldName, $this->compare, $this->value);
        }

        return $model->where($this->fieldName, $this->value);
    }
}
