<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByIdOrSlug implements CriteriaInterface
{
    /**
     * @var null
     */
    private $param;

    /**
     * @var
     */
    private $idColumn;

    /**
     * @var
     */
    private $slugColumn;

    /**
     * @var boolean
     */
    private $forceSlug;

    /**
     * FilterByIdOrSlug constructor.`
     *
     * @param null   $param
     * @param string $idColumn
     * @param string $slugColumn
     * @param boolean $forceSlug
     */
    public function __construct($param = null, $idColumn = 'id', $slugColumn = 'slug', $forceSlug = false)
    {
        $this->param = $param;
        $this->idColumn = $idColumn;
        $this->slugColumn = $slugColumn;
        $this->forceSlug = $forceSlug;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (! $this->param) {
            return $model;
        }

        if ($this->forceSlug) {
            return $model->where($this->slugColumn, $this->param);
        }

        // If value is number
        if (ctype_digit($this->param)) {
            return $model->where($this->idColumn, $this->param);
        }

        // If value is string
        return $model->where($this->slugColumn, $this->param);
    }
}
