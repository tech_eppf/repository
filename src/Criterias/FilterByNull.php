<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByNull implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $field;

    /**
     * FilterByNull constructor.
     *
     * @param string $field
     */
    public function __construct($field = '')
    {
        $this->field = $field;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNull($this->field);
    }
}
