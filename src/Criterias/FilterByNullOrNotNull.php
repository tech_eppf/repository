<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByNullOrNotNull implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var bool
     */
    protected $isNull;

    /**
     * FilterByFieldNull constructor.
     *
     * @param $fieldName
     * @param bool $isNull
     */
    public function __construct($fieldName, $isNull = true)
    {
        $this->fieldName = $fieldName;
        $this->isNull = $isNull;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->isNull && (! empty($this->fieldName))) {
            return $model->whereNull($this->fieldName);
        }

        if ((! $this->isNull) && (! empty($this->fieldName))) {
            return $model->whereNotNull($this->fieldName);
        }

        return $model;
    }
}
