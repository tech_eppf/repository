<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class OrderByRaw implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $sortColumn;

    /**
     * @var string
     */
    protected $sortOrder;

    /**
     * OrderByRawCriteria constructor.
     *
     * @param string $sortColumn
     * @param string $sortOrder
     */
    public function __construct($sortColumn = '', $sortOrder = 'ASC')
    {
        $this->sortColumn = $sortColumn;
        $this->sortOrder = $sortOrder;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->sortColumn)) {
            return $model;
        }

        if ($this->sortColumn == 'available_time_only') {
            return $model->orderByRaw("HOUR(available_date) $this->sortOrder, MINUTE(available_date) $this->sortOrder");
        } elseif ($this->sortColumn == 'random') {
            return $model->inRandomOrder();
        } elseif (is_array($this->sortColumn)) {
            foreach ($this->sortColumn as $order) {
                $model->orderBy($order['column'], $order['order']);
            }

            return $model;
        } else {
            return $model->orderBy($this->sortColumn, $this->sortOrder);
        }
    }
}
