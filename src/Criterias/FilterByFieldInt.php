<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByFieldInt implements CriteriaInterface
{
    /**
     * @var null
     */
    protected $value;

    /**
     * @var
     */
    protected $field;

    /**
     * @var string
     */
    protected $condition;

    /**
     * FilterByFieldInt constructor.
     * @param null $value
     * @param $field
     * @param string $condition
     */
    public function __construct($value = null, $field, $condition = '=')
    {
        $this->value = $value;
        $this->field = $field;
        $this->condition = $condition;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!isset($this->value)) {
            return $model;
        }

        return $model->where($this->field, $this->condition, $this->value);
    }
}
