<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByJsonField implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $column;

    /**
     * FilterByFieldString constructor.
     * @param string $value
     * @param string $column
     */
    public function __construct($value, $column)
    {
        $this->value = $value;
        $this->column = $column;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->value) || empty($this->column)) {
            return $model;
        }

        return $model->whereJsonContains($this->column, $this->value);
    }
}
