<?php

namespace Elidev\Repository\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByNotIn implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $arrayValue;

    /**
     * @var string
     */
    protected $field;

    /**
     * FilterByNotIn constructor.
     * @param array $arrayValue
     * @param string $field
     */
    public function __construct($arrayValue = [], $field = '')
    {
        $this->arrayValue = $arrayValue;
        $this->field = $field;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->arrayValue) || empty($this->field)) {
            return $model;
        }

        return $model->whereNotIn($this->field, $this->arrayValue);
    }
}
