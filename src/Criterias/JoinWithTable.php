<?php

namespace Elidev\Repository\Criterias;


use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class JoinWithTable implements CriteriaInterface
{
    /**
     * @var string
     */
    private $withTable;

    /**
     * @var string
     */
    private $withKey;

    /**
     * @var string
     */
    private $localKey;

    /**
     * @var string
     */
    private $localTable;

    /**
     * @var string
     */
    private $joinType;

    /**
     * JoinWithTable constructor.
     *
     * @param $withTable
     * @param $withKey
     * @param $localTable
     * @param $localKey
     * @param string $joinType
     */
    public function __construct($withTable, $withKey, $localTable, $localKey = 'id', $joinType = 'join')
    {
        $this->withTable = $withTable;
        $this->withKey = $withKey;
        $this->localTable = $localTable;
        $this->localKey = $localKey;
        $this->joinType = $joinType;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->{$this->joinType}($this->withTable, function ($join) {
            $join->on($this->localTable . '.' . $this->localKey, '=', $this->withTable . '.' . $this->withKey);
        });
    }
}