== v1.0.2 - 24 Feb, 2020 ==
* Can clean cache

== v1.0.2 - 23 Feb, 2020 ==
* Add repository cache, supports cache for getLimitWithPageOffset

== v1.0.1 - 18 Jan, 2020 ==
* adds firstOrCreate, firstOrNew